import axios from "axios";


export const formData = {
  state: () => ({
    name: '',
    phone: '',
    email: '',
    city: ''
  }),
  getters: {

  },
  mutations: {
    setName(state, name) {
      state.name = name;
    },
    setPhone(state, phone) {
      state.phone = phone;
    },
    setEmail(state, email) {
      state.email = email;
    },
    setCity(state, city) {
      state.city = city;
    },
  },
  actions: {
    async submitRequest({state}, request) {
      const url = 'http://hh.autodrive-agency.ru/test-tasks/front/task-7/';
      try {
        const resp = await axios.post(url, request);
        return resp.data;
      } catch (error) {
        return error.response;
      }
    },
  },
  namespaced: true,
}
