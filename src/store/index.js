import {createStore} from "vuex";
import {formData} from "@/store/formData"

export default createStore({
  state: {
    response: ''
  },
  mutations: {
    setResponse(state, response) {
      state.response = response;
    },
  },
  modules: {
    form: formData
  }
})